// The content of this document between the tag directives is automatically included in the following pages: medicaid:2980.adoc, snap:3010.adoc, tanf:1008.adoc
= 3402 Voter Registration
:navtitle: Voter Registration
// tag::manual-insert[]
:policy-manual-title: National Voter Registration Act (NVRA)
:chapter-number: 3400
:reviewed-date:
:next-review-date:
:effective-date: February 2022
:policy-number: 3402
:policy-title: Voter Registration
:previous-policy-number: FS Policy 3010, MA Policy 2980, TANF Policy 1008

include::ROOT:partial$policy-header.adoc[]

== Background

Congress enacted the National Voter Registration Act of 1993 (NVRA), which established requirements designed to afford individuals who apply for and receive public assistance an opportunity to register to vote at the point of application, recertification/renewal, change of address, and upon request by a customer who wants to register to vote. 52 U.S.C. § 20501 et seq.
(Also, see the section of Georgia's law on the “Registration of Voters” found at O.C.G.A. § 21-2-222).

== Requirements

Consistent with the NVRA and Georgia law, the Department of Human Services (DHS), Division of Family and Children Services (DFCS) staff and its' providers are required to distribute to Food Stamp (FS), Medicaid (MA), and Temporary Assistance for Needy Families (TANF) customers the following documents – *at application, recertification/renewal, and when a change of address is reported*:

. a Voter Registration Application form; and
. the Voter Registration Information (formerly called the Declaration Statement), which states the following:
+
--
“If you are not registered to vote where you live now, would you like to apply to register to vote here today? +
pass:[____] Yes +
pass:[____] No +
pass:[____] I do not want to answer the Voter Registration question

Applying to register or declining to register to vote will not affect the amount of assistance that you will be provided by this agency.
If you would like help in filling out the voter registration application form, we will help you.
The decision whether to seek or accept help is yours.
You may fill out the application form in private.
If you believe that someone has interfered with your right to register or to decline to register to vote, your right to privacy in deciding whether to register or in applying to register to vote, or your right to choose your own political party or other political preference, you may file a complaint with the Secretary of State at: 2 Martin Luther King Jr. Drive, Suite 802, West Tower, Atlanta, GA 30334 or by calling 404-656-2871.
IF YOU DO NOT CHECK EITHER BOX, YOU WILL BE CONSIDERED TO HAVE DECIDED NOT TO REGISTER TO VOTE AT THIS TIME.

A copy of the Georgia Voter Registration application is included with DFCS applications, renewals, and change of address forms.
You can also request a Voter Registration application from your caseworker.
If you complete a Voter Registration application, submit it to the Georgia Secretary of State's Office following the instructions provided on the Voter Registration application.”
--

== Procedures

The following forms were updated to incorporate the Voter Registration Information within the form itself.
*Prior versions of the forms should be disposed of and replaced with the most current versions*:

* Form 297 (Application for Benefits) *(version 9/20 or later)*;
* Form 298 (Application for Senior SNAP) *(version 7/21 or later)*;
* Form 508 (Food Stamp/Medicaid/TANF Renewal Form) *(version 9/20 or later)*;
* Form 846 (Change Report Form) *(version 7/21 or later)*;
* Form 94 (Medicaid-only Application) *(version 1/22 or later)*;
* Form 94A (Medicaid Streamlined Application) *(version 1/22 or later)*; and
* Form 700 (Application for Medicaid and Medicare Savings for Qualified Beneficiaries) *(version 1/22 or later)*.

*When providing customers with the forms listed above, please ensure that a “STATE OF GEORGIA APPLICATION FOR VOTER REGISTRATION” is attached.*

NOTE: The NVRA packet [i.e., the NVRA cover letter and Declaration Statement] should no longer be provided separately with the forms referenced above, since the Voter Registration Information is now incorporated within these forms.

Workers must read the Voter Registration Information, as it appears in Gateway, when completing an application, renewal/recertification, or change of address (for example, when completing a telephonic application).
If the interaction involves a change of address, then the worker must ask the Voter Registration Information to any applicant or recipient who is of the legal voting age, and is either on the telephone or in-person, for that individual to answer on his/her own behalf.

Upon receiving a returned Voter Registration Information page *or* any version of a completed paper application, renewal, or change report form, locate the customer's response provided to the question: “If you are not registered to vote where you live now, would you like to apply to register to vote here today?” _(Note: This question is a part of the Voter Registration Information section)._ Caseworkers are required to document each customer's response to the Voter Registration Information section on the appropriate screens in the Gateway worker portal.

The DFCS NVRA Coordinator at the State Office will be responsible for maintaining and transmitting a monthly report to the Georgia Secretary of State's Office, which will provide customer responses to the Voter Registration Information captured in Gateway.
The monthly report will be maintained by the DFCS NVRA Coordinator for at least 24-months.

*VOTER REGISTRATION APPLICATIONS*: County DFCS staff must advise customers who want to submit a Voter Registration Application to complete and transmit the application to the Georgia Secretary of State's Office using the instructions provided on the Voter Registration Application.

*At any time, upon request, county DFCS staff must provide customers a Voter Registration Application and assist customers with completing and mailing the Voter Registration Application*.
If customers ask that DFCS staff mail the Voter Registration Application for them (or if we otherwise receive a Voter Registration Application via fax, mail, or drop box), staff must mail the customer's Voter Registration Application to the Georgia Secretary of State's Office, upon receipt, using the instructions found on the Voter Registration Application.

*DFCS STAFF SHALL NOT:*

* seek to influence or discourage a customer's political preference or party registration
* display any such political preference or party allegiance
* make any statement to an applicant or take any action to discourage the customer from registering to vote
* make any statement to a customer or take any action that the customer's decision to register or not to register has any bearing on the customer's application for or receipt of public assistance benefits
* hold completed voter registration applications

NOTE: Upon receipt, staff must mail completed Voter Registration Applications using the self-addressed stamped envelope provided with the Voter Registration Application.

== Additional Voter Registration Information for Customers

*Register Online:* To apply to register to vote where you live now, visit https://sos.ga.gov.

*Print an application*: You may print an application by visiting https://sos.ga.gov.

If you want a Georgia Voter Registration application mailed to you, you may call the Georgia Secretary of State's office at 404-656-2871, call DFCS' Customer Contact Center at 877- 423-4746, or visit https://sos.ga.gov.

== Voter Registration Application

Each office should keep a two-week supply of Voter Registration Applications.
The Voter Registration Applications can be downloaded from the Secretary of State by visiting https://sos.ga.gov.

NOTE: DFCS staff shall not alter Voter Registration Applications in any way.
Voter Registration Applications must be provided to DFCS customers in the same format as provided by the Georgia Secretary of State's Office.

== Confidential Information and Records

Information and records that are considered confidential for Voter Registration purposes:

* Identifying information and records about a public assistance applicant or recipient, including but not limited to: the individual's name, date of birth, address, telephone number, Social Security Number, driver's license or state identification, driver's license number and customer case files.
* A customer's response to the Voter Registration Information section, which is a document included within the customer's case file.

NOTE: All confidentiality laws, rules, and policies that involve public assistance applicant and recipient information and records also apply to Voter Registration information and records that are maintained by DHS/DFCS.

== Getting Help

Please contact the Georgia Secretary of State Office for assistance:

[%hardbreaks]
Secretary of State Elections Division
2 Martin Luther King Jr. Drive
Suite 802 West Tower
Atlanta, Georgia 30334
Email: gaelections@sos.ga.gov

Inquiries regarding the status of a Voter Registration Application should be directed to the Georgia Secretary of State Office.
Additional voter registration information can be obtained by visiting the Secretary of State Elections Division at: https://www.sos.ga.gov.
// end::manual-insert[]